package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	/**
	 * Operation that gets the minimum integer number found in the bag. If the bag is empty it returns -1.<br>
	 * @param bag Bag of Integers<br>
	 * @return min THe minimum integer found or -1.
	 */
	public int getMin (IntegersBag bag)
	{
		int min=Integer.MAX_VALUE;
		int value;
		if(bag!=null)
		{
			Iterator <Integer> it = bag.getIterator();
			if(!it.hasNext()) return -1;
			while(it.hasNext())
			{
				value=it.next();
				if(value<min)
				{
					min=value;
				}
			}
		}
		return min;
	}
	/**
	 * Gets all the values that are more than or equal to a given number from the bag of integers.<br>
	 * @param value Given value.<br>
	 * @param bag Integers bag.<br>
	 * @return A new bag of integers that can be empty.
	 */
	public IntegersBag numbersMoreEqual(int value, IntegersBag bag)
	{
		IntegersBag b = new IntegersBag();
		Iterator<Integer> iter = bag.getIterator();
		while(iter.hasNext())
		{
			int v=iter.next();
			if(v>=value)
			{
				b.addDatum(v);
			}
		}
		return b;
	}
	
	/**
	 * Gets all the values that are less than a given number from the bag of integers.<br>
	 * @param value Given value.<br>
	 * @param bag Integers bag.<br>
	 * @return A new bag of integers that can be empty.
	 */
	public IntegersBag numbersLessThan(int value, IntegersBag bag)
	{
		IntegersBag b = new IntegersBag();
		Iterator<Integer> iter = bag.getIterator();
		while(iter.hasNext())
		{
			int v=iter.next();
			if(v<value)
			{
				b.addDatum(v);
			}
		}
		return b;
	}
}
